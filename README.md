# fluent-plugin-timescaledb - fork plugin https://github.com/1500cloud/fluent-plugin-timescaledb

[Fluentd](https://fluentd.org/) output plugin to store logs in TimescaleDB

## Installation

You will probably need some Ruby tools available (e.g., `ruby-dev`) as well as
the libpq library (`libpq-dev`/`postgres-dev`, depending on your OS).

### Via specific_install

```
$ gem install specific_install \
&& gem specific_install https://gitlab.com/forks_72/fluent-plugin-timescaledb.git
```

## Configuration

You must have a running instance of [TimescaleDB](https://www.timescale.com/) somewhere.

Create a database and then a table within it, something like this:

```sql
SET timezone = 'UTC';
CREATE EXTENSION timescaledb;
CREATE EXTENSION timescaledb_toolkit;
SELECT default_version, installed_version FROM pg_available_extensions where name = 'timescaledb';
SELECT default_version, installed_version FROM pg_available_extensions where name = 'timescaledb_toolkit';

create table IF NOT EXISTS log_records
(
  time TIMESTAMPTZ NOT NULL,
  tag    CHAR(128) NOT NULL,
  record JSONB     NOT NULL
);

SELECT create_hypertable('log_records', 'time', chunk_time_interval => INTERVAL '1 day',if_not_exists => TRUE);
SELECT add_retention_policy('log_records', INTERVAL '3 months');

```

At your pleasure, you may want to add more indexes inside the JSONB block to
allow for better querying. That's left as an exercise for the reader,
depending on your exact needs.

To configure FluentD, add a block similar to this to your config.

```
<match **>
  @type timescaledb
  db_conn_string "host=localhost user=fluent password=supersecret dbname=fluent"
</match>
```

The exact value of db_conn_string is as defined by the `pg` Gem.

## Copyright

* Copyright a.v.galushko86@gmail.com 2023
* License
  * Apache License, Version 2.0

## Copyright Original 
* Copyright(c) 2019: 1500 Services Ltd
* License
  * Apache License, Version 2.0
