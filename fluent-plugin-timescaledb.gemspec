Gem::Specification.new do |s|
  s.name        = 'fluent-plugin-timescaledb'
  s.version     = '1.1.0'
  s.license     = 'Apache-2.0'
  s.summary     = "Write Fluent logs to TimescaleDB via promethues format"
  s.description = "Write Fluent logs to TimescaleDB via promethues format"
  s.authors     = ["Human"]
  s.email       = 'a.v.galushko86@gmail.com'
  s.homepage    = 'https://gitlab.com/forks_72/fluent-plugin-timescaledb'
  s.files       = ["lib/fluent/plugin/out_timescaledb.rb"]

  s.add_runtime_dependency 'pg', '~> 1.1'
end
